namespace ServerMapExporter.Render.Ore
{
    using System.Drawing;
    using System.Threading.Tasks;
    using ServerMapExporter.Models;

    public interface IOreMapRenderer
    {
        Task<Bitmap> RenderOreMapData(ChunkColumnExport data, string code);
        Task RenderOreMapDataPart(ChunkColumnExport data, string code, Bitmap bitmap, int offsetX, int offsetY);
    }
}
