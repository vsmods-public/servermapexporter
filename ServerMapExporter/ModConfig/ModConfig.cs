namespace ServerMapExporter.ModConfig
{
    using System;
    using Foundation.ModConfig;
    using CommandSystem2.Entities.Interfaces;
    using Microsoft.Extensions.DependencyInjection;
    using Newtonsoft.Json;

    public class ModConfig : ModConfigBase, ICommandSystemConfig
    {
        //Foundation config:
        public override string ModCode => "servermapexporter";

        //Command System config:
        [JsonIgnore]
        public IServiceProvider Services { get; set; } = new ServiceCollection().BuildServiceProvider(true);
        [JsonIgnore]
        public bool IgnoreExtraArguments => false;

        //Servermapexporter config:
        public string ExportPath { get; set; } = "tiles/";

        public bool IsLiveUpdateActive { get; set; } = false;
        public int LiveUpdateConcurrency { get; set; } = 1;
        public bool ExportHeightMap { get; set; } = false;
        public bool ExportOreMap { get; set; } = true;
        public bool ShouldUseSeasonalColors { get; set; } = true;

        public int SaveOccurenceInMinutes { get; set; } = 10;
    }
}
