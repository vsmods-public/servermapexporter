namespace ServerMapExporter.ModSystem
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Vintagestory.API.Client;
    using Vintagestory.API.Common;
    using Vintagestory.API.MathTools;

    public class ClientCommands : ModSystem
    {
        private ICoreClientAPI _capi;

        public override bool ShouldLoad(EnumAppSide forSide)
        {
            return forSide == EnumAppSide.Client;
        }

        public override void StartClientSide(ICoreClientAPI api)
        {
            this._capi = api;

            api.RegisterCommand("exportcolor", "Creates a color file to be used for the server-side mapping", null, OnExportColorCommand);
        }

        private void OnExportColorCommand(int groupId, CmdArgs args)
        {
            if (args.Length < 1)
            {
                //SendSyntaxMessage(player);
                return;
            }

            switch (args.PopWord("all"))
            {
                case "all":
                    ExportColorData(groupId, args);
                    break;
                case "this":
                    var pos = _capi.World.Player.Entity.Pos.AsBlockPos.Copy();
                    var isRainmapHeight = args.PopBool(true).Value;
                    if (isRainmapHeight)
                    {
                        pos.Y = _capi.World.BlockAccessor.GetRainMapHeightAt(_capi.World.Player.Entity.Pos.AsBlockPos);
                    }

                    var block = _capi.World.BlockAccessor.GetBlock(pos);
                    var useSeasonalColor = args.PopBool(false).Value;
                    GetColor(useSeasonalColor, block, out var red, out var green, out var blue);
                    _capi.ShowChatMessage($"At { (isRainmapHeight ? "rainmap height" : "your height") }, there is a { block.Code.ToString() } " +
                        $"which returns color of '{ColorUtil.ToRgba(255, red, green, blue)}'");

                    break;
                default:
                    break;
            }
        }

        private void ExportColorData(int groupId, CmdArgs args)
        {
            var blockIdColorDict = new Dictionary<string, int[]>();
            var failIds = new List<int>();
            var useSeasonalColor = args.PopBool(false).Value;
            var variations = 4;

            foreach (var block in _capi.World.Blocks.Where(x => x != null && x.Code != null))
            {
                var colors = new int[variations];
                try
                {
                    for (var i = 0; i < variations; i++)
                    {
                        GetColor(useSeasonalColor, block, out var red, out var green, out var blue);
                        colors[i] = ColorUtil.ToRgba(255, red, green, blue);
                    }

                    blockIdColorDict.Add(block.Code.ToString(), colors);
                }
                catch (Exception)
                {
                    failIds.Add(block.Id);
                }
            }

            var output = JsonConvert.SerializeObject(blockIdColorDict);
            File.WriteAllText("colors" + (useSeasonalColor ? "_season" : "") + ".json", output);

            _capi.ShowChatMessage($"Exported {blockIdColorDict.Count} blocks and {failIds.Count} failed");
        }

        private void GetColor(bool useSeasonalColor, Block block, out int red, out int green, out int blue)
        {
            var color = 0;
            var clientPos = _capi.World.Player.Entity.Pos.XYZInt;

            if (useSeasonalColor)
            {
                var avgCol = block.GetColor(_capi, clientPos.AsBlockPos);
                var rndCol = block.GetRandomColor(_capi, clientPos.AsBlockPos, BlockFacing.UP);
                color = ColorUtil.ColorOverlay(avgCol, rndCol, 0.125f);
            }
            else
            {
                color = block.GetColorWithoutTint(_capi, clientPos.AsBlockPos);
                if (block.BlockMaterial == EnumBlockMaterial.Leaves ||
                    block.BlockMaterial == EnumBlockMaterial.Plant ||
                    block.BlockMaterial == EnumBlockMaterial.Soil
                )
                {
                    color = _capi.World.ApplyColorMapOnRgba(block.ClimateColorMapForMap, block.SeasonColorMapForMap, color, clientPos.X, clientPos.Y, clientPos.Z);
                    var greenAmp = ColorUtil.ColorG(127);
                    color = ColorUtil.ColorOverlay(color, greenAmp, 0.25f);
                }
            }

            var packedFormat = ColorUtil.ColorMultiply3Clamped(color, 1);

            red = ColorUtil.ColorB(packedFormat);
            green = ColorUtil.ColorG(packedFormat);
            blue = ColorUtil.ColorR(packedFormat);
        }
    }
}
