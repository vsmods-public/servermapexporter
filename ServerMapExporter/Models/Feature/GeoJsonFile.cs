namespace ServerMapExporter.Models.Feature
{
    using System.Collections.Generic;
    using System.IO;
    using Foundation.Utils;
    using Newtonsoft.Json;

    public class GeoJsonFile<T1, T2, T3> where T1 : IGeoJsonFeature<T2, T3> where T2 : IGeoJsonGeometry<T3>
    {
        [JsonProperty("type")]
        public string Type = "FeatureCollection";
        [JsonProperty("name")]
        public string Name;
        public Dictionary<string, object> crs = new Dictionary<string, object>() { { "type", "name" }, { "properties", new Dictionary<string, string>() { { "name", "urn:ogc:def:crs:EPSG::3857" } } } };
        [JsonProperty("features")]
        public List<T1> Features { get; set; }

        public GeoJsonFile(string name)
        {
            this.Name = name;
        }

        public void Save(string path)
        {
            FileUtils.CreateFolderIfNotExists(Path.GetDirectoryName(path));
            File.WriteAllText(path, JsonConvert.SerializeObject(this));
        }
    }
}
