namespace ServerMapExporter.Models.Feature
{
    using System.Collections.Generic;

    public class TranslocatorGeoJsonFile : GeoJsonFile<TranslocatorFeature, LineStringGeometry, int[][]>
    {
        public TranslocatorGeoJsonFile(List<TranslocatorFeature> translocators = null)
            : base("translocator")
        {
            this.Features = translocators ?? new List<TranslocatorFeature>();
        }
    }
}
