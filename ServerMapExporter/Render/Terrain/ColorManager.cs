namespace ServerMapExporter.Render.Terrain
{
    using System.Collections.Generic;
    using System.IO;
    using Foundation.Utils;
    using ModConfig;
    using Newtonsoft.Json;
    using Vintagestory.API.MathTools;

    public class ColorManager
    {
        private readonly Dictionary<string, int[]> blockCodeToColorDict;

        public ColorManager(ModConfig config, bool useSeasonalColor = true)
        {
            var path = Path.Combine(config.ExportPath, "colors" + (useSeasonalColor ? "_season" : "") + ".json");
            this.blockCodeToColorDict = JsonConvert.DeserializeObject<Dictionary<string, int[]>>(File.ReadAllText(path));
        }

        public int GetColorFromBlockCode(string code)
        {
            if (this.blockCodeToColorDict.TryGetValue(code, out var colorOptions))
            {
                return colorOptions[RandomUtils.Random.Next(0, colorOptions.Length)];
            }

            return ColorUtil.ToRgba(255, 255, 0, 0);
        }
    }
}
