namespace ServerMapExporter.ModSystem
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Foundation.StaticStorage;
    using ModConfig;
    using ServerMapExporter.Models;
    using ServerMapExporter.Render;
    using ServerMapExporter.Render.Feature;
    using Vintagestory.API.Server;
    using Vintagestory.ServerMods;

    public class ChunkExportConsumer
    {
        private readonly ConcurrentQueue<GroupedChunkColumnExport> queue;
        private readonly IServerWorldAccessor world;
        private readonly List<IFeatureRender> features;

        private RenderManager renderManager { get; }
        public ModConfig Config { get; }
        private IWorldManagerAPI WorldManager { get; }
        private Task worker { get; }

        public ChunkExportConsumer(RenderManager renderManager, ConcurrentQueue<GroupedChunkColumnExport> queue, CancellationToken token, ModConfig config, IWorldManagerAPI worldManager, IServerWorldAccessor world, GenDeposits genDepositsModSystem, List<IFeatureRender> features, ChunkExportConsumerGroup group = null, System.Action<GroupedChunkColumnExport> onWorkFinished = null, System.Action<GroupedChunkColumnExport> onWorkFailed = null)
        {
            this.renderManager = renderManager;
            this.queue = queue;
            this.Config = config;
            this.WorldManager = worldManager;
            this.world = world;
            this.features = features;
            this.worker = Task.Run(async () =>
            {
                while (!token.IsCancellationRequested)
                {
                    var hasWork = this.queue.TryDequeue(out var workItem);
                    if (!hasWork)
                    {
                        //We got no work yet, just chill
                        await Task.Delay(1000);
                        continue;
                    }

                    var result = false;
                    try
                    {
                        var liveItem = workItem as LiveChunkExport;
                        if (liveItem != null)
                        {
                            result = await this.DoTheWork(liveItem);
                        }
                    }
                    catch (Exception)
                    {
                        result = false;
                    }
                    finally
                    {
                        workItem.HasBeenRendered = true;
                    }

                    if (!result)
                    {
                        if (onWorkFailed != null)
                        {
                            onWorkFailed(workItem);
                        }

                        StaticStorage.Subtract("currentRendering");
                    }

                    if (group != null)
                    {
                        group.Notify();
                    }

                    if (onWorkFinished != null)
                    {
                        onWorkFinished(workItem);
                    }
                }
            }, token);
        }

        private async Task<bool> DoTheWork(LiveChunkExport workItem)
        {
            StaticStorage.Subtract("currentEnqueued");
            StaticStorage.Add("currentRendering");

            var terrainRender = this.renderManager.GetTerrainRenderer();
            foreach (var chunkColumnExport in workItem.ChunkDict.Values.Cast<ChunkRawColumnExport>())
            {
                var chunkOffsetX = chunkColumnExport.ChunkCoords.X % 8;
                var chunkOffsetZ = chunkColumnExport.ChunkCoords.Y % 8;

                var result = await terrainRender.RenderTerrainDataPart(chunkColumnExport, null, chunkOffsetX, chunkOffsetZ);
                if (!result)
                {
                    return false;
                }

                foreach (var feature in this.features)
                {
                    object tempData = null;
                    if ((tempData = await feature.ShouldExecute(chunkColumnExport, this.world)) != null)
                    {
                        await feature.Execute(chunkColumnExport, tempData, this.world);
                    }
                }

                /*if ((be = chunk.Chunks.Select(x => {
                    if (x.BlockEntities.Count > 0)
                    {

                    }
                    var a = x.BlockEntities?.Values?.FirstOrDefault(bee => bee as BlockEntityStaticTranslocator != null);
                    if(a != null)
                    {

                    }
                    return a;
                }).FirstOrDefault()) != null)
                {

                }*/

                /*var chunkColumnTranslocatorAreas = chunkColumnExport.ChunkColumn.MapRegion.GeneratedStructures.Where(structure => structure.Code.ToLower().Equals("gates") && chunkColumnCuboid.Contains(structure.Location.Center.ToBlockPos()));
                if (chunkColumnTranslocatorAreas.Any())
                {
                    foreach (var translocatorArea in chunkColumnTranslocatorAreas)
                    {
                        BlockPos foundPos = null;
                        BlockStaticTranslocator translocator = null;
                        this.world.BlockAccessor.SearchBlocks(translocatorArea.Location.Start.AsBlockPos, translocatorArea.Location.End.AsBlockPos, (block, pos) =>
                        {
                            BlockStaticTranslocator transBlock = block as BlockStaticTranslocator;
                            if (transBlock != null && transBlock.Repaired)
                            {
                                foundPos = pos;
                                translocator = transBlock;
                                return false;
                            }

                            return true;
                        });

                        if (translocator == null)
                            continue;

                        var entity = this.world.BlockAccessor.GetBlockEntity(foundPos) as BlockEntityStaticTranslocator;

                    }
                }*/
                //var chunkColumnTraderAreas = chunkColumnExport
            }

            StaticStorage.Subtract("currentRendering");
            return true;
        }
    }
}
