namespace ServerMapExporter.Render.Terrain
{
    using System.Drawing;
    using System.Threading.Tasks;
    using ServerMapExporter.Models;

    public interface ITerrainRenderer
    {
        Task<Bitmap> RenderTerrainData(ChunkColumnExport data);
        Task<bool> RenderTerrainDataPart(ChunkColumnExport data, Bitmap bitmap, int offsetX, int offsetY);
    }
}
