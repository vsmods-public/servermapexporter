namespace ServerMapExporter.Render
{
    using ModConfig;
    using ServerMapExporter.Render.Ore;
    using ServerMapExporter.Render.Terrain;
    using Vintagestory.API.Common;
    using Vintagestory.API.Server;

    public class RenderManager
    {
        private readonly ModConfig config;
        private readonly ITerrainRenderer currentTerrainRender;
        private readonly ColorManager colorManager;
        private readonly IOreMapRenderer currentOreMapRender;

        public RenderManager(int chunkSize, IBlockAccessor blockAccessor, IWorldManagerAPI worldManager, ModConfig config)
        {
            this.config = config;
            this.colorManager = new ColorManager(this.config);
            if (this.config.ExportHeightMap)
            {
                this.currentTerrainRender = new FlatChunkRenderer(chunkSize, blockAccessor, this.colorManager);
            }
            else
            {
                this.currentTerrainRender = new ShadedRawChunkRenderer(chunkSize, blockAccessor, this.colorManager);
            }
            this.currentOreMapRender = new FlatOreMapRenderer(chunkSize, worldManager);
        }

        public ITerrainRenderer GetTerrainRenderer()
        {
            return this.currentTerrainRender;
        }

        public IOreMapRenderer GetOreMapRender()
        {
            return this.currentOreMapRender;
        }
    }
}
