namespace ServerMapExporter.Render.Feature
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using ModConfig;
    using Newtonsoft.Json;
    using ServerMapExporter.Models;
    using ServerMapExporter.Models.Feature;
    using Vintagestory.API.MathTools;
    using Vintagestory.API.Server;
    using Vintagestory.GameContent;

    public class TranslocatorRender : IFeatureRender
    {
        private readonly ModConfig config;
        private readonly int mapSize;
        private readonly int chunkSize;
        private readonly TranslocatorGeoJsonFile translocatorData;
        private readonly ConcurrentDictionary<long, TranslocatorFeature> translocatorDict;
        private readonly string FILEPATH;

        public TranslocatorRender(ModConfig config, int mapSize, int chunkSize = 32)
        {
            this.config = config;
            this.mapSize = mapSize;
            this.chunkSize = chunkSize;

            this.FILEPATH = Path.Combine(this.config.ExportPath, "raw/features/translocators.geojson");
            if (File.Exists(this.FILEPATH))
            {
                using (var reader = new StreamReader(File.OpenRead(this.FILEPATH)))
                {
                    this.translocatorData = JsonConvert.DeserializeObject<TranslocatorGeoJsonFile>(reader.ReadToEnd());
                }
            }
            else
            {
                this.translocatorData = new TranslocatorGeoJsonFile();
            }

            this.translocatorDict = new ConcurrentDictionary<long, TranslocatorFeature>(this.translocatorData.Features.ToDictionary(traderRow => MapUtil.Index2dL(traderRow.X1, traderRow.Y1, this.mapSize)));
        }

        public async Task Execute(ChunkColumnExport data, object executeData, IServerWorldAccessor world)
        {
            var translocators = executeData as IEnumerable<BlockEntityStaticTranslocator>;
            if (translocators == null)
            {
                throw new ArgumentNullException(nameof(executeData));
            }

            foreach (var activeTranslocator in translocators)
            {
                var entrancePos = activeTranslocator.Pos;
                var spawnCorrectedEntranceX = entrancePos.X - world.DefaultSpawnPosition.AsBlockPos.X;
                var spawnCorrectedEntranceZ = -1 * (entrancePos.Z - world.DefaultSpawnPosition.AsBlockPos.Z);

                var exitPos = activeTranslocator.TargetLocation;
                var spawnCorrectedExitX = exitPos.X - world.DefaultSpawnPosition.AsBlockPos.X;
                var spawnCorrectedExitZ = -1 * (exitPos.Z - world.DefaultSpawnPosition.AsBlockPos.Z);

                if (this.translocatorDict.ContainsKey(MapUtil.Index2dL(spawnCorrectedEntranceX, spawnCorrectedEntranceZ, this.mapSize)) || this.translocatorDict.ContainsKey(MapUtil.Index2dL(spawnCorrectedExitX, spawnCorrectedExitZ, this.mapSize)))
                {
                    continue;
                }

                this.translocatorDict.GetOrAdd(MapUtil.Index2dL(spawnCorrectedEntranceX, spawnCorrectedEntranceZ, this.mapSize), new TranslocatorFeature(spawnCorrectedEntranceX, spawnCorrectedEntranceZ, spawnCorrectedExitX, spawnCorrectedExitZ));
            }
        }

        public async Task Save()
        {
            this.translocatorData.Features = this.translocatorDict.Values.ToList();
            this.translocatorData.Save(this.FILEPATH);
        }

        public async Task<object> ShouldExecute(ChunkColumnExport data, IServerWorldAccessor world)
        {
            var activeTranslocators = data.Chunks.Select(chunk => chunk.BlockEntities.Where(entityKVP => entityKVP.Value is BlockEntityStaticTranslocator).Select(translocatorBEKVP => translocatorBEKVP.Value as BlockEntityStaticTranslocator)).SelectMany(x => x);
            if (activeTranslocators.Any())
            {
                return activeTranslocators;
            }

            return null;

            /*var chunkColumnCuboid = new Cuboidi(data.ChunkCoords.X, 0, data.ChunkCoords.Y, data.ChunkCoords.X + this.chunkSize, this.mapSize, data.ChunkCoords.Y + this.chunkSize);
            var chunkColumnTranslocatorAreas = data.ChunkColumn.MapRegion.GeneratedStructures.Where(structure => structure.Code.ToLower().Equals("gates") && chunkColumnCuboid.IntersectsOrTouches(structure.Location));
            List<BlockEntityStaticTranslocator> repairedTranslocatorsInChunk = new List<BlockEntityStaticTranslocator>();
            foreach (var translocatorArea in chunkColumnTranslocatorAreas)
            {
                BlockPos foundPos = null;
                BlockStaticTranslocator translocator = null;
                world.BlockAccessor.SearchBlocks(translocatorArea.Location.Start.AsBlockPos, translocatorArea.Location.End.AsBlockPos, (block, pos) =>
                {
                    BlockStaticTranslocator transBlock = block as BlockStaticTranslocator;
                    if (transBlock != null && transBlock.Repaired)
                    {
                        foundPos = pos;
                        translocator = transBlock;
                        return false;
                    }

                    return true;
                });

                if (translocator == null)
                    continue;

                var entity = world.BlockAccessor.GetBlockEntity(foundPos) as BlockEntityStaticTranslocator;
                if (entity == null)
                    continue;

                repairedTranslocatorsInChunk.Add(entity);
            }

            return repairedTranslocatorsInChunk.Count > 0 ? repairedTranslocatorsInChunk : null;*/
        }
    }
}
