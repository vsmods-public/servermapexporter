namespace ServerMapExporter.Render.Feature
{
    using System.Collections.Concurrent;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using ModConfig;
    using Newtonsoft.Json;
    using ServerMapExporter.Models;
    using ServerMapExporter.Models.Feature;
    using Vintagestory.API.Server;
    using Vintagestory.GameContent;

    public class TraderRender : IFeatureRender
    {
        private readonly ModConfig config;
        private readonly int mapSizeX;
        private readonly int chunkSize;
        private readonly TraderGeoJsonFile traderData;
        private readonly ConcurrentDictionary<long, TraderFeature> traderDict;
        private readonly string FILEPATH;
        private readonly int mapSizeY;

        public TraderRender(ModConfig config, int mapSizeX, int mapSizeY = 256, int chunkSize = 32)
        {
            this.config = config;
            this.mapSizeX = mapSizeX;
            this.mapSizeY = mapSizeY;
            this.chunkSize = chunkSize;

            this.FILEPATH = Path.Combine(this.config.ExportPath, "raw/features/traders.geojson");
            if (File.Exists(this.FILEPATH))
            {
                using (var reader = new StreamReader(File.OpenRead(this.FILEPATH)))
                {
                    this.traderData = JsonConvert.DeserializeObject<TraderGeoJsonFile>(reader.ReadToEnd());
                }
            }
            else
            {
                this.traderData = new TraderGeoJsonFile();
            }

            this.traderDict = new ConcurrentDictionary<long, TraderFeature>(this.traderData.Features.ToDictionary(traderRow => traderRow.EntityId));
        }

        public async Task Execute(ChunkColumnExport data, object executeData, IServerWorldAccessor world)
        {
            /*var chunkColumnTraders = executeData as IEnumerable<GeneratedStructure>;
            if (chunkColumnTraders == null)
                throw new ArgumentNullException(nameof(executeData));

            foreach (var traderStructure in chunkColumnTraders)
            {
                var spawnCorrectedX = traderStructure.Location.Center.AsBlockPos.X - world.DefaultSpawnPosition.AsBlockPos.X;
                var spawnCorrectedZ = -1 * (traderStructure.Location.Center.AsBlockPos.Z - world.DefaultSpawnPosition.AsBlockPos.Z);

                var traderEntity = data.Chunks.Where(x => x.Entities != null && x.Entities.Any()).SelectMany(x => x.Entities).FirstOrDefault(entity => entity.Code.Path.StartsWith("humanoid-trader"));
                if( traderEntity == null )
                {
                    //var relatedChunks = traderStructure.Location.

                }

                traderDict.GetOrAdd(MapUtil.Index2dL(spawnCorrectedX, spawnCorrectedZ, this.mapSizeX), new TraderFeature(spawnCorrectedX, spawnCorrectedZ, traderEntity?.Code.Path.Split('-').Last() ?? "Commodities"));*/
            var traders = executeData as EntityTrader[];
            foreach (var trader in traders)
            {
                this.traderDict.GetOrAdd(trader.EntityId, new TraderFeature(trader.SidedPos.AsBlockPos.X, trader.SidedPos.AsBlockPos.Z, trader.Code.Path.Split('-').Last() ?? "Commodities", trader.EntityId.ToString()));
            }
        }

        public async Task Save()
        {
            /*if (!this.traderData.Features.Select(traderRow => MapUtil.Index2dL(traderRow.X, traderRow.Y, this.mapSizeX)).Except(this.traderDict.Keys).Any())
            {
                return;
            }*/

            this.traderData.Features = this.traderDict.Values.ToList();
            this.traderData.Save(this.FILEPATH);
        }

        public async Task<object> ShouldExecute(ChunkColumnExport data, IServerWorldAccessor world)
        {
            /*var chunkColumnCuboid = new Cuboidi(data.ChunkCoords.X * this.chunkSize, 0, data.ChunkCoords.Y * this.chunkSize, (data.ChunkCoords.X + 1) * this.chunkSize, this.mapSizeY, (data.ChunkCoords.Y + 1) * this.chunkSize);
            var foundTraders = data.ChunkColumn.MapRegion.GeneratedStructures.Where(structure => chunkColumnCuboid.Contains(structure.Location.Center.ToBlockPos()) && (structure.Group?.ToLower().Equals("trader") ?? false));

            return foundTraders.Count() > 0 ? foundTraders : null;*/
            var foundEntities = data.Chunks.Where(x => x.Entities != null && x.Entities.Any()).SelectMany(x => x.Entities);
            var foundTraders = foundEntities.Where(entity => entity?.Code?.Path?.StartsWith("humanoid-trader") ?? false);

            return foundTraders?.Any() ?? false ? foundTraders.Select(entity => entity as EntityTrader).ToArray() : null;
        }
    }
}
