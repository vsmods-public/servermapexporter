namespace ServerMapExporter.Models
{
    using System;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.Runtime.InteropServices;

    public class BitmapContainer : IDisposable, IEquatable<BitmapContainer>
    {
        public int[] PixelData { get; set; }
        public Bitmap Bitmap { get; set; }

        private GCHandle Handle { get; set; }
        private IntPtr Address { get; set; }

        public BitmapContainer(int imageWidth, int imageHeight, Bitmap bitmap)
        {
            var fmt = PixelFormat.Format32bppRgb;
            var pixelFormatSize = Image.GetPixelFormatSize(fmt);
            var stride = imageWidth * pixelFormatSize;
            var padding = 32 - stride % 32;
            if (padding < 32)
            {
                stride += padding;
            }

            this.PixelData = new int[stride / 32 * imageHeight + 1];
            this.Handle = GCHandle.Alloc(this.PixelData, GCHandleType.Pinned);
            this.Address = Marshal.UnsafeAddrOfPinnedArrayElement(this.PixelData, 0);

            var data = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadWrite, PixelFormat.Format32bppRgb);
            Marshal.Copy(data.Scan0, this.PixelData, 0, this.PixelData.Length - 1);

            this.Bitmap = new Bitmap(imageWidth, imageHeight, stride / 8, fmt, this.Address);
        }

        public void Dispose()
        {
            this.Address = IntPtr.Zero;
            if (this.Handle.IsAllocated)
            {
                this.Handle.Free();
            }

            this.Bitmap.Dispose();
            this.Bitmap = null;
            this.PixelData = null;
        }

        public static bool operator ==(BitmapContainer obj1, BitmapContainer obj2)
        {
            if (ReferenceEquals(obj1, obj2))
            {
                return true;
            }
            if (ReferenceEquals(obj1, null))
            {
                return false;
            }
            if (ReferenceEquals(obj2, null))
            {
                return false;
            }

            return obj1.Equals(obj2);
        }

        public static bool operator !=(BitmapContainer obj1, BitmapContainer obj2)
        {
            return !(obj1 == obj2);
        }

        public bool Equals(BitmapContainer other)
        {
            if (ReferenceEquals(other, null))
            {
                return false;
            }
            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return this.Handle.Equals(other.Handle)
                && this.Address.Equals(other.Address)
                && this.Bitmap.Equals(other.Bitmap);
        }

        public override bool Equals(object obj)
        {
            return this.Equals(obj as BitmapContainer);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = this.Handle.GetHashCode();
                hashCode = hashCode * 397 ^ this.Address.GetHashCode();
                hashCode = hashCode * 397 ^ this.Bitmap.GetHashCode();
                return hashCode;
            }
        }
    }
}
