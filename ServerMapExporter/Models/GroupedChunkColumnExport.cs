namespace ServerMapExporter.Models
{
    using System.Collections.Generic;

    public class GroupedChunkColumnExport
    {
        public Dictionary<long, ChunkColumnExport> ChunkDict { get; set; }
        public int ZoomLevel { get; set; }
        public long CornerChunkIndex { get; set; }
        public long OriginatingChunkIndex { get; set; }
        public bool ExportOreMap { get; set; }
        public bool ExportHeightMap { get; set; }
        public bool HasBeenRendered { get; set; }

        public GroupedChunkColumnExport()
        {
            this.ChunkDict = new Dictionary<long, ChunkColumnExport>();
        }
    }
}
