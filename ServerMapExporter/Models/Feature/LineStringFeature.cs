namespace ServerMapExporter.Models.Feature
{
    using System.Collections.Generic;

    public class LineStringFeature : IGeoJsonFeature<LineStringGeometry, int[][]>
    {
        public string Type => "Feature";
        public Dictionary<string, object> Properties { get; set; }
        public LineStringGeometry Geometry { get; set; }

        public LineStringFeature(int x1, int y1, int x2, int y2)
        {
            this.Properties = new Dictionary<string, object>();
            this.Geometry = new LineStringGeometry(x1, y1, x2, y2);
        }
    }
}
