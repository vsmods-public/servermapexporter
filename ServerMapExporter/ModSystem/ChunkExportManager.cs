namespace ServerMapExporter.ModSystem
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Threading;
    using System.Threading.Tasks;
    using Foundation.StaticStorage;
    using Foundation.Utils;
    using ModConfig;
    using ServerMapExporter.Models;
    using ServerMapExporter.Render;
    using ServerMapExporter.Render.Feature;
    using Vintagestory.API.Common;
    using Vintagestory.API.MathTools;
    using Vintagestory.API.Server;
    using Vintagestory.ServerMods;

    public class ChunkExportManager
    {
        private readonly IWorldManagerAPI worldManager;
        private readonly RenderManager renderManager;
        private readonly ICoreServerAPI api;
        private readonly ModConfig config;
        private readonly ConcurrentQueue<GroupedChunkColumnExport> queue;
        private readonly Dictionary<Guid, ChunkExportConsumerGroup> keyToGroupDict;
        private readonly GenDeposits genDepositSystem;
        private readonly List<IFeatureRender> features;

        private readonly int zoomLevels = 2;

        public delegate void WorkItemFinishedEventHandler(object sender, EventArgs e);
        public static event WorkItemFinishedEventHandler OnWorkItemFinished;

        public static ConcurrentDictionary<long, BitmapContainer> chunkIndexToBitmapDict = new ConcurrentDictionary<long, BitmapContainer>();

        private readonly Task saveTask;
        public DateTime waitStart;

        public ChunkExportManager(int concurrency, IWorldManagerAPI worldManager, RenderManager renderManager, ICoreServerAPI api, ModConfig config)
        {
            this.worldManager = worldManager;
            this.renderManager = renderManager;
            this.api = api;
            this.config = config;
            this.queue = new ConcurrentQueue<GroupedChunkColumnExport>();
            this.keyToGroupDict = new Dictionary<Guid, ChunkExportConsumerGroup>();
            this.genDepositSystem = api.ModLoader.GetModSystem<GenDeposits>();

            this.features = new List<IFeatureRender>()
            {
                new TraderRender(this.config, this.worldManager.MapSizeX, this.worldManager.MapSizeY, this.worldManager.ChunkSize),
                new TranslocatorRender(this.config, this.worldManager.MapSizeX, this.worldManager.ChunkSize)
            };

            api.Event.ChunkColumnLoaded += this.HandleLoadedChunkColumn;

            if (concurrency > 0)
            {
                this.AddConsumers(concurrency);
            }

            this.saveTask = Task.Run(async () =>
            {
                while (true)
                {
                    this.waitStart = DateTime.UtcNow;

#if DEBUG
                    await Task.Delay(10000);
#else
                    await Task.Delay(this.config.SaveOccurenceInMinutes * 60000);
#endif

                    var toBeSaved = Interlocked.Exchange(ref chunkIndexToBitmapDict, new ConcurrentDictionary<long, BitmapContainer>());
                    // We have to wait until everything in the queue, from when we swap the dictionaries, finish rendering
                    // Otherwise we will potentially discard rendered chunks
                    var lastInQueue = this.queue.LastOrDefault();
                    var stopSpinning = DateTime.UtcNow.AddSeconds(10);
                    SpinWait.SpinUntil(() => lastInQueue?.HasBeenRendered ?? true || DateTime.UtcNow > stopSpinning);
                    lock (toBeSaved)
                    {
                        foreach (var bitmapKVP in toBeSaved)
                        {
                            try
                            {
                                MapUtilsEx.ChunkPosFromChunkIndex2DL(bitmapKVP.Key, this.worldManager.MapSizeX, out var chunkX, out var chunkZ);
                                var path = Path.Combine(this.config.ExportPath, "raw/terrain/1/", $"tile_{chunkX * this.worldManager.ChunkSize}_{chunkZ * this.worldManager.ChunkSize}.png");
                                Bitmap existingBitmap = null;
                                if (File.Exists(path))
                                {
                                    using (var reader = File.OpenRead(path))
                                    {
                                        existingBitmap = new Bitmap(reader);
                                    }

                                    for (var x = 0; x < this.worldManager.ChunkSize * 8; x++)
                                    {
                                        for (var y = 0; y < this.worldManager.ChunkSize * 8; y++)
                                        {
                                            var newPixel = bitmapKVP.Value.Bitmap.GetPixel(x, y);
                                            var oldPixel = existingBitmap.GetPixel(x, y);

                                            bitmapKVP.Value.Bitmap.SetPixel(x, y, newPixel == Color.FromArgb(255, 0, 0, 0) ? oldPixel : newPixel);
                                        }
                                    }
                                }

                                bitmapKVP.Value.Bitmap.Save(path);
                                if (existingBitmap != null)
                                {
                                    existingBitmap.Dispose();
                                    existingBitmap = null;
                                }
                            }
                            catch (Exception)
                            {
                                //Awkward, but we just save it next time!
                                //TODO: Make this fully thread safe, so a bitmap cannot be locked at the same time as it tries to get updated by a thread.
                            }
                        }

                        for (var i = 2; i <= this.zoomLevels; i++)
                        {
                            var zoomPower = (int)Math.Pow(2, i - 1);

                            var groupedTiles = toBeSaved
                            .Select(x =>
                            {
                                MapUtilsEx.ChunkPosFromChunkIndex2DL(x.Key, this.worldManager.MapSizeX, out var chunkX, out var chunkZ);
                                var cornerChunkX = chunkX - chunkX % (zoomPower * 8);
                                var cornerChunkZ = chunkZ - chunkZ % (zoomPower * 8);
                                return new { chunkX, chunkZ, cornerChunkX, cornerChunkZ, container = x.Value };
                            })
                            .GroupBy(x => MapUtil.Index2dL(x.cornerChunkX, x.cornerChunkZ, this.worldManager.MapSizeX))
                            .OrderBy(x => x.First().cornerChunkX)
                            .ThenBy(x => x.First().cornerChunkZ);
                            foreach (var group in groupedTiles)
                            {
                                var cornerChunkX = group.First().cornerChunkX;
                                var cornerChunkZ = group.First().cornerChunkZ;
                                var terrainBitmap = new Bitmap(this.worldManager.ChunkSize * zoomPower * 8, this.worldManager.ChunkSize * zoomPower * 8);
                                for (var x = 0; x < zoomPower; x++)
                                {
                                    for (var y = 0; y < zoomPower; y++)
                                    {
                                        Bitmap existingBitmap = null;
                                        var newChunkX = cornerChunkX + x * 8;
                                        var newChunkZ = cornerChunkZ + y * 8;
                                        if ((existingBitmap = group.FirstOrDefault(tiles => tiles.chunkX == newChunkX && tiles.chunkZ == newChunkZ)?.container.Bitmap) == null)
                                        {
                                            var path = Path.Combine(this.config.ExportPath, "raw/terrain/1/", $"tile_{newChunkX * this.worldManager.ChunkSize}_{newChunkZ * this.worldManager.ChunkSize}.png");
                                            if (File.Exists(path))
                                            {
                                                using (var reader = File.OpenRead(path))
                                                {
                                                    var fileName = Path.GetFileName(path);
                                                    var regex = Regex.Match(fileName, @"(\d+)_(\d+)");
                                                    var chunkX = int.Parse(regex.Groups[1].Value);
                                                    var chunkZ = int.Parse(regex.Groups[2].Value);

                                                    existingBitmap = new Bitmap(reader);
                                                }
                                            }
                                            else
                                            {
                                                existingBitmap = new Bitmap(this.worldManager.ChunkSize * 8, this.worldManager.ChunkSize * 8);
                                                using (var graphics = Graphics.FromImage(existingBitmap))
                                                {
                                                    graphics.Clear(Color.FromArgb(0, 35, 132, 235));
                                                }
                                            }
                                        }

                                        for (var bitmapX = 0; bitmapX < existingBitmap.Width; bitmapX++)
                                        {
                                            for (var bitmapY = 0; bitmapY < existingBitmap.Height; bitmapY++)
                                            {
                                                terrainBitmap.SetPixel(x * existingBitmap.Width + bitmapX, y * existingBitmap.Height + bitmapY, existingBitmap.GetPixel(bitmapX, bitmapY));
                                            }
                                        }
                                    }
                                }

                                var resizedTerrainBitmap = Render.Utils.ImageUtils.HighQualityResizeImage(terrainBitmap, this.worldManager.ChunkSize * 8, this.worldManager.ChunkSize * 8);
                                resizedTerrainBitmap.Save(Path.Combine(this.config.ExportPath, "raw/terrain/", i.ToString(), $"tile_{cornerChunkX * this.worldManager.ChunkSize}_{cornerChunkZ * this.worldManager.ChunkSize}.png"));
                                if (terrainBitmap != null)
                                {
                                    terrainBitmap.Dispose();
                                    terrainBitmap = null;
                                }
                                if (resizedTerrainBitmap != null)
                                {
                                    resizedTerrainBitmap.Dispose();
                                    resizedTerrainBitmap = null;
                                }
                            }
                        }

                        foreach (var bitmap in toBeSaved.Values)
                        {
                            bitmap.Dispose();
                        }
                    }
                    toBeSaved = null;

                    foreach (var feature in this.features)
                    {
                        await feature.Save();
                    }
                }
            });
        }

        public ChunkExportConsumerGroup AddConsumers(int additional = 1, System.Func<bool> stopPredicate = null)
        {
            var group = new ChunkExportConsumerGroup();
            for (var i = 0; i < additional; i++)
            {
                group.Consumers.Add(new ChunkExportConsumer(this.renderManager, this.queue, group.TokenSource.Token, this.config, this.worldManager, this.api.World, this.genDepositSystem, this.features, group,
                    (finishedWorkItem) =>
                    {
                        if (stopPredicate != null && stopPredicate())
                        {
                            group.TokenSource.Cancel();
                        }
                    },
                    (failedWorkItem) =>
                    {
                        StaticStorage.Add("exportFailed");

                        MapUtilsEx.ChunkPosFromChunkIndex2DL(failedWorkItem.CornerChunkIndex, this.worldManager.MapSizeX, out var chunkX, out var chunkZ);
                        this.Enqueue(chunkX, chunkZ);
                    }));
            }
            this.keyToGroupDict[group.Key] = group;

            return group;
        }

        public void Enqueue(int chunkX, int chunkZ)
        {
            var loadedChunk = this.worldManager.GetChunk(chunkX, 0, chunkZ);
            if (loadedChunk == null)
            {
                lock (this.worldManager)
                {
                    this.worldManager.LoadChunkColumn(chunkX, chunkZ, false);
                }
            }
            else
            {
                this.HandleLoadedChunkColumn(new Vec2i(chunkX, chunkZ),
                    Enumerable.Range(0, this.worldManager.MapSizeY / this.worldManager.ChunkSize)
                    .Select(chunkY => this.worldManager.GetChunk(chunkX, chunkY, chunkZ) as IWorldChunk)
                    .ToArray());
            }
        }

        private void HandleLoadedChunkColumn(Vec2i chunkCoord, IWorldChunk[] chunks)
        {
            if (!this.config.IsLiveUpdateActive)
            {
                return;
            }

            var chunkSize = this.worldManager.ChunkSize;
            var chunkX = chunkCoord.X;
            var chunkZ = chunkCoord.Y;
            var chunkOffsetX = chunkX % 8;
            var chunkOffsetZ = chunkZ % 8;
            var cornerChunkX = chunkX - chunkOffsetX;
            var cornerChunkZ = chunkZ - chunkOffsetZ;

            var liveData = new LiveChunkExport()
            {
                ExportHeightMap = this.config.ExportHeightMap,
                ExportOreMap = this.config.ExportOreMap,
                CornerChunkIndex = MapUtil.Index2dL(cornerChunkX, cornerChunkZ, this.worldManager.MapSizeX),
                OriginatingChunkIndex = MapUtil.Index2dL(chunkX, chunkZ, this.worldManager.MapSizeX),
                ZoomLevel = zoomLevels,
            };

            liveData.ChunkDict.Add(liveData.OriginatingChunkIndex, new ChunkRawColumnExport()
            {
                ChunkColumn = this.worldManager.GetMapChunk(chunkCoord.X, chunkCoord.Y),
                ChunkCoords = chunkCoord,
                Chunks = chunks,
            });

            var bitmap = new Bitmap(chunkSize * 1 * 8, chunkSize * 1 * 8);
            using (var graphics = Graphics.FromImage(bitmap))
            {
                graphics.Clear(Color.FromArgb(0, 35, 132, 235));
            }

            var bitmapContainer = new BitmapContainer(chunkSize * 1 * 8, chunkSize * 1 * 8, bitmap);
            var existingBitmap = chunkIndexToBitmapDict.GetOrAdd(MapUtil.Index2dL(cornerChunkX, cornerChunkZ, this.worldManager.MapSizeX), bitmapContainer);
            if (existingBitmap != bitmapContainer)
            {
                bitmapContainer.Dispose();
                bitmapContainer = null;
            }

            liveData.DataContainer = existingBitmap;
            (liveData.ChunkDict.First().Value as ChunkRawColumnExport).DataContainer = existingBitmap;

            StaticStorage.Add("currentEnqueued");
            this.queue.Enqueue(liveData);
        }
    }
}
