namespace ServerMapExporter.Models.Feature
{
    using System;
    using Newtonsoft.Json;

    public class TranslocatorFeature : LineStringFeature
    {
        [JsonIgnore]
        public string Label => Convert.ToString(this.Properties["Label"]);
        [JsonIgnore]
        public int X1 => Convert.ToInt32(this.Geometry.Coordinates[0][0]);
        [JsonIgnore]
        public int Y1 => Convert.ToInt32(this.Geometry.Coordinates[0][1]);
        [JsonIgnore]
        public int X2 => Convert.ToInt32(this.Geometry.Coordinates[1][0]);
        [JsonIgnore]
        public int Y2 => Convert.ToInt32(this.Geometry.Coordinates[1][1]);

        public TranslocatorFeature(int x1, int y1, int x2, int y2, string label = default)
            : base(x1, y1, x2, y2)
        {
            if (label != default)
            {
                this.Properties.Add("Label", label);
            }
        }
    }
}
