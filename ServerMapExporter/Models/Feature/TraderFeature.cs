namespace ServerMapExporter.Models.Feature
{
    using System;
    using Newtonsoft.Json;

    public class TraderFeature : PointFeature
    {
        [JsonIgnore]
        public long EntityId => long.Parse(Convert.ToString(this.Properties["ENTITYID"]));
        [JsonIgnore]
        public int X => Convert.ToInt32(this.Properties["X"]);
        [JsonIgnore]
        public int Y => Convert.ToInt32(this.Properties["Y"]);

        public TraderFeature(int x, int y, string type, string entityId)
            : base(x, y)
        {
            this.Properties.Add("TYPE", type);
            this.Properties.Add("ENTITYID", entityId);
        }
    }
}
