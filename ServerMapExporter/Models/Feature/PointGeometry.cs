namespace ServerMapExporter.Models.Feature
{
    public class PointGeometry : IGeoJsonGeometry<int[]>
    {
        public string Type => "Point";
        public int[] Coordinates { get; set; }

        public PointGeometry(int x, int y)
        {
            this.Coordinates = new[] { x, y };
        }
    }
}
