namespace ServerMapExporter.Render.Ore
{
    using System;
    using System.Drawing;
    using System.Threading.Tasks;
    using ServerMapExporter.Models;
    using Vintagestory.API.MathTools;
    using Vintagestory.API.Server;
    using Vintagestory.ServerMods;

    public class FlatOreMapRenderer : IOreMapRenderer
    {
        private readonly int chunkSize;
        private readonly IWorldManagerAPI worldManager;

        public FlatOreMapRenderer(int chunkSize, IWorldManagerAPI worldManager)
        {
            this.chunkSize = chunkSize;
            this.worldManager = worldManager;
        }

        public async Task<Bitmap> RenderOreMapData(ChunkColumnExport data, string code)
        {
            if (data.Chunks == null || data.Chunks.Length == 0)
            {
                throw new ArgumentNullException(nameof(data));
            }

            var bitmap = new Bitmap(this.chunkSize, this.chunkSize);

            var regionSize = this.worldManager.RegionSize;
            var regionMap = this.worldManager.GetMapRegion(data.ChunkCoords.X * this.chunkSize / regionSize, data.ChunkCoords.Y * this.chunkSize / regionSize);
            if (regionMap == null || !regionMap.OreMaps.TryGetValue(code, out var regionOreMap))
            {
                return bitmap;
            }

            var noiseSize = regionSize / TerraGenConfig.oreMapScale;
            for (var x = 0; x < this.chunkSize; x++)
            {
                for (var z = 0; z < this.chunkSize; z++)
                {
                    var blockX = (data.ChunkCoords.X * this.chunkSize + x) % regionSize;
                    var blockZ = (data.ChunkCoords.Y * this.chunkSize + z) % regionSize;

                    var posXInRegionOre = GameMath.Clamp((float)blockX / regionSize * noiseSize, 0, noiseSize - 1);
                    var posZInRegionOre = GameMath.Clamp((float)blockZ / regionSize * noiseSize, 0, noiseSize - 1);

                    var oreValue = regionOreMap.GetUnpaddedColorLerped(posXInRegionOre, posZInRegionOre);

                    bitmap.SetPixel(x, z, Color.FromArgb(oreValue >> 16 & 0xff, oreValue >> 8 & 0xff, oreValue & 0xff));
                }
            }

            return bitmap;
        }

        public async Task RenderOreMapDataPart(ChunkColumnExport data, string code, Bitmap bitmap, int offsetX, int offsetY)
        {
            var chunkRender = await this.RenderOreMapData(data, code);
            for (var x = 0; x < chunkRender.Width; x++)
            {
                for (var y = 0; y < chunkRender.Height; y++)
                {
                    bitmap.SetPixel(offsetX + x, offsetY + y, chunkRender.GetPixel(x, y));
                }
            }
        }
    }
}
