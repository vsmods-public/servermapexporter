namespace ServerMapExporter.Models.Feature
{
    using Newtonsoft.Json;

    public interface IGeoJsonGeometry<T>
    {
        [JsonProperty("type")]
        string Type { get; }
        [JsonProperty("coordinates")]
        T Coordinates { get; }
    }
}
