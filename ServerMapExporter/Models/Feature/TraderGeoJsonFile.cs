namespace ServerMapExporter.Models.Feature
{
    using System.Collections.Generic;

    public class TraderGeoJsonFile : GeoJsonFile<TraderFeature, PointGeometry, int[]>
    {
        public TraderGeoJsonFile(List<TraderFeature> traders = null)
            : base("traders_new")
        {
            this.Features = traders ?? new List<TraderFeature>();
        }
    }
}
