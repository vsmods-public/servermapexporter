namespace ServerMapExporter.ModSystem
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;

    public class ChunkExportConsumerGroup
    {
        public Guid Key { get; }
        public List<ChunkExportConsumer> Consumers { get; }
        public CancellationTokenSource TokenSource { get; }

        public ChunkExportConsumerGroup()
        {
            this.Key = Guid.NewGuid();
            this.Consumers = new List<ChunkExportConsumer>();
            this.TokenSource = new CancellationTokenSource();
        }

        public void Notify()
        {
            this.OnNotify?.Invoke(this, new NotifyEventArgs());
        }

        public delegate void OnNotifyEventHandler(object sender, NotifyEventArgs e);
        public event OnNotifyEventHandler OnNotify;
    }
}

public class NotifyEventArgs
{
}
