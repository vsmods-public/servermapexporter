namespace ServerMapExporter.Models.Feature
{
    using System.Collections.Generic;

    public class PointFeature : IGeoJsonFeature<PointGeometry, int[]>
    {
        public string Type => "Feature";
        public Dictionary<string, object> Properties { get; set; }
        public PointGeometry Geometry { get; set; }

        public PointFeature(int x, int y)
        {
            this.Properties = new Dictionary<string, object>()
            {
                { "X", x },
                { "Y", y }
            };

            this.Geometry = new PointGeometry(x, y);
        }
    }
}
