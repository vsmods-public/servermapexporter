namespace ServerMapExporter.ModSystem
{
    using System;
    using System.Drawing;
    using System.IO;
    using System.Text.RegularExpressions;
    using Foundation.ModConfig;
    using Foundation.StaticStorage;
    using Foundation.Utils;
    using ModConfig;
    using ServerMapExporter.Models;
    using ServerMapExporter.Render;
    using Vintagestory.API.Common;
    using Vintagestory.API.MathTools;
    using Vintagestory.API.Server;
    using Vintagestory.ServerMods;

    public class ServerMapExporterSystem : ModSystem
    {
        private ICoreServerAPI sapi;
        private ChunkExportManager chunkExportManager;
        private RenderManager renderManager;

        private ChunkExportConsumerGroup liveExportGroup = null;

        public ModConfig Config { get; private set; }

        public override bool ShouldLoad(EnumAppSide forSide)
        {
            return forSide == EnumAppSide.Server;
        }

        public override double ExecuteOrder()
        {
            return 2;
        }

        public override void StartServerSide(ICoreServerAPI api)
        {
            this.sapi = api;
            this.Config = api.LoadOrCreateConfig<ModConfig>(this);
            FileUtils.CreateFolderIfNotExists(this.Config.ExportPath);

            this.renderManager = new RenderManager(this.sapi.WorldManager.ChunkSize, this.sapi.World.BlockAccessor, this.sapi.WorldManager, this.Config);
            this.chunkExportManager = new ChunkExportManager(0, this.sapi.WorldManager, this.renderManager, this.sapi, this.Config);

            api.RegisterCommand("toggleliveexport", "Starts up the live export of data from the game as chunks are updated, in real time", "", this.OnToggleLiveExportCommand);
            api.RegisterCommand("exportstatus", "", "", this.OnExportStatusCommand);
            api.RegisterCommand("loadalltiles", "", "", this.OnLoadAllTilesCommand);

            //Totally hacky but can't be arsed:
            if (this.Config.IsLiveUpdateActive)
            {
                this.Config.IsLiveUpdateActive = false;
                this.OnToggleLiveExportCommand(null, -1, new CmdArgs());
            }

            this.sapi.Event.InitWorldGenerator(() =>
            {
                var genDepositSystem = this.sapi.ModLoader.GetModSystem<GenDeposits>();
                for (var i = 1; i <= 2; i++)
                {
                    Directory.CreateDirectory(Path.Combine(this.Config.ExportPath, "raw/terrain/", i.ToString()));
                    if (this.Config.ExportOreMap)
                    {
                        foreach (var deposit in genDepositSystem.Deposits)
                        {
                            Directory.CreateDirectory(Path.Combine(this.Config.ExportPath, "raw/ore/", deposit.Code, i.ToString()));
                        }
                    }
                }
            }, "standard");

        }

        private void OnLoadAllTilesCommand(IServerPlayer player, int groupId, CmdArgs args)
        {
            foreach (var path in Directory.EnumerateFiles(Path.Combine(this.Config.ExportPath, "raw/terrain/1/")))
            {
                var dict = ChunkExportManager.chunkIndexToBitmapDict;
                lock (dict)
                {
                    using (var reader = File.OpenRead(path))
                    {
                        var fileName = Path.GetFileName(path);
                        var regex = Regex.Match(fileName, @"(\d+)_(\d+)");
                        var chunkX = int.Parse(regex.Groups[1].Value);
                        var chunkZ = int.Parse(regex.Groups[2].Value);

                        dict.AddOrUpdate(MapUtil.Index2dL(chunkX, chunkZ, this.sapi.WorldManager.MapSizeX), new BitmapContainer(256, 256, new Bitmap(reader)), (index, container) => container);
                    }
                }
            }

            player.SendMessage(groupId, "All tiles from zoom level 1 loaded succesfully!", EnumChatType.CommandSuccess);
        }

        private void OnExportStatusCommand(IServerPlayer player, int groupId, CmdArgs args)
        {
            var finalMessage = "";

            var totalCount = StaticStorage.GetValue<int>("totalCount");
            if (totalCount > 0)
            {
                finalMessage = $"A full export of {totalCount} chunks was triggered. That's {totalCount / 256} tiles. {StaticStorage.GetValue<int>("fullExportDoneCount")} were done.\n ";
            }

            finalMessage += $"Current tiles waiting for chunks to load: {StaticStorage.GetValue<int>("currentWaiting")}.\n";
            finalMessage += $" Current tiles waiting to be rendered: {StaticStorage.GetValue<int>("currentEnqueued")}.\n";
            finalMessage += $" Current tiles being rendered: {StaticStorage.GetValue<int>("currentRendering")}.\n";
            finalMessage += $" Current tiles failed: {StaticStorage.GetValue<int>("exportFailed")}.\n";
            var futureTime = DateTime.UtcNow + (DateTime.UtcNow - this.chunkExportManager.waitStart);
            var timeDifference = futureTime - DateTime.UtcNow;
            finalMessage += $" Time since last save:{((int)timeDifference.TotalMinutes == 0 ? "" : ((int)timeDifference.TotalMinutes) + " minute(s) and")} {timeDifference.Seconds} seconds.\n";
            player.SendMessage(groupId, finalMessage, EnumChatType.CommandSuccess);
        }

        private void OnToggleLiveExportCommand(IServerPlayer player, int groupId, CmdArgs args)
        {
            var consumers = args.PopInt(this.Config.LiveUpdateConcurrency).Value;
            if (this.Config.IsLiveUpdateActive)
            {
                this.liveExportGroup.TokenSource.Cancel();
                this.liveExportGroup = null;
                this.Config.IsLiveUpdateActive = false;
            }
            else
            {
                var liveGroup = this.chunkExportManager.AddConsumers(consumers);
                this.liveExportGroup = liveGroup;
                this.Config.IsLiveUpdateActive = true;
                this.Config.LiveUpdateConcurrency = consumers;
            }

            this.Config.Save(this.sapi);

            if (player == null)
            {
                return;
            }

            player.SendMessage(groupId, $"Auto toggle is now {(this.Config.IsLiveUpdateActive ? $"active with {consumers} threads." : "disabled")}.", EnumChatType.CommandSuccess);
        }
    }
}
