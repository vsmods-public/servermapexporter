namespace ServerMapExporter.Models
{
    using System.Drawing.Imaging;

    public class LiveChunkExport : GroupedChunkColumnExport
    {
        public byte[] DataBuffer;
        public int DataDepth;
        public BitmapData Data { get; internal set; }

        public BitmapContainer DataContainer { get; set; }

        public LiveChunkExport()
        {
        }
    }
}
