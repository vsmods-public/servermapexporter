namespace ServerMapExporter.Models.Feature
{
    public class LineStringGeometry : IGeoJsonGeometry<int[][]>
    {
        public string Type => "LineString";
        public int[][] Coordinates { get; set; }

        public LineStringGeometry(int x1, int y1, int x2, int y2)
        {
            this.Coordinates = new[] { new[] { x1, y1 }, new[] { x2, y2 } };
        }
    }
}
