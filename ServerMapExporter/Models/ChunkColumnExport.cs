namespace ServerMapExporter.Models
{
    using Vintagestory.API.Common;
    using Vintagestory.API.MathTools;

    public class ChunkColumnExport
    {
        public Vec2i ChunkCoords { get; set; }
        public IWorldChunk[] Chunks { get; set; }
        public IMapChunk ChunkColumn { get; set; }
    }
}
