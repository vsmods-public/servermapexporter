namespace ServerMapExporter.Render.Feature
{
    using System.Threading.Tasks;
    using ServerMapExporter.Models;
    using Vintagestory.API.Server;

    public interface IFeatureRender
    {
        Task<object> ShouldExecute(ChunkColumnExport data, IServerWorldAccessor world);
        Task Execute(ChunkColumnExport data, object executeData, IServerWorldAccessor world);
        Task Save();
    }
}
