namespace ServerMapExporter.Models.Feature
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    public interface IGeoJsonFeature<T, T2> where T : IGeoJsonGeometry<T2>
    {
        [JsonProperty("type")]
        string Type { get; }
        [JsonProperty("properties")]
        Dictionary<string, object> Properties { get; }
        [JsonProperty("geometry")]
        T Geometry { get; }
    }
}
