namespace ServerMapExporter.Render.Terrain
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using System.Threading.Tasks;
    using ServerMapExporter.Models;
    using Vintagestory.API.Common;
    using Vintagestory.API.MathTools;

    public class ShadedRawChunkRenderer : ITerrainRenderer
    {
        private readonly int chunkSize;
        private readonly IBlockAccessor blockAccessor;
        private readonly ColorManager colorManager;

        public ShadedRawChunkRenderer(int chunkSize, IBlockAccessor blockAccessor, ColorManager colorManager)
        {
            this.chunkSize = chunkSize;
            this.blockAccessor = blockAccessor;
            this.colorManager = colorManager;
        }

        public async Task<Bitmap> RenderTerrainData(ChunkColumnExport data)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> RenderTerrainDataPart(ChunkColumnExport data, Bitmap bitmap, int offsetX, int offsetY)
        {
            var realData = data as ChunkRawColumnExport;
            if (realData == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            var testList = new List<int>();
            for (var x = 0; x < this.chunkSize; x++)
            {
                for (var z = 0; z < this.chunkSize; z++)
                {
                    var blockPositionX = data.ChunkCoords.X * this.chunkSize + x;
                    var blockPositionZ = data.ChunkCoords.Y * this.chunkSize + z;
                    var yMax = data.ChunkColumn.RainHeightMap[MapUtil.Index2d(x, z, this.chunkSize)];
                    testList.Add(yMax);

                    Block block = null;
                    try
                    {
                        if (data.Chunks[yMax / this.chunkSize].Blocks == null || data.Chunks[yMax / this.chunkSize].Blocks.Length == 0)
                        {
                            data.Chunks[yMax / this.chunkSize].Unpack();
                        }

                        block = this.blockAccessor.GetBlock(data.Chunks[yMax / this.chunkSize].Blocks[MapUtil.Index3d(x, yMax % this.chunkSize, z, this.chunkSize, this.chunkSize)]);

                        while (block.Code.ToString().Contains("game:air") || block.Code.ToString().Contains("snowlayer") || block.Code.ToString().Contains("snowblock"))
                        {
                            if (data.Chunks[--yMax / this.chunkSize].Blocks == null || data.Chunks[yMax / this.chunkSize].Blocks.Length == 0)
                            {
                                data.Chunks[yMax / this.chunkSize].Unpack();
                            }

                            block = this.blockAccessor.GetBlock(data.Chunks[yMax / this.chunkSize].Blocks[MapUtil.Index3d(x, yMax % this.chunkSize, z, this.chunkSize, this.chunkSize)]);
                        }
                    }
                    catch (Exception)
                    {
                        return false;
                    }

                    var color = this.colorManager.GetColorFromBlockCode(block.Code.ToString());

                    var westYHeight = x - 1 < 0 || z - 1 < 0 ? this.blockAccessor.GetRainMapHeightAt(blockPositionX - 1, blockPositionZ - 1) : data.ChunkColumn.RainHeightMap[MapUtil.Index2d(x - 1, z - 1, this.chunkSize)];
                    var eastYHeight = x - 1 < 0 || z + 1 >= this.chunkSize ? this.blockAccessor.GetRainMapHeightAt(blockPositionX - 1, blockPositionZ + 1) : data.ChunkColumn.RainHeightMap[MapUtil.Index2d(x - 1, z + 1, this.chunkSize)];
                    var northYHeight = x + 1 >= this.chunkSize || z - 1 < 0 ? this.blockAccessor.GetRainMapHeightAt(blockPositionX + 1, blockPositionZ - 1) : data.ChunkColumn.RainHeightMap[MapUtil.Index2d(x + 1, z - 1, this.chunkSize)];
                    var southYHeight = 0;

                    var heightDifferences = new[] { westYHeight, eastYHeight, northYHeight, southYHeight };
                    var heightFactor = heightDifferences.Sum(height => Math.Sign(yMax - height));

                    var slopeBoost = 1f;
                    var slopeness = Math.Sign(heightFactor);
                    if (slopeness > 0)
                    {
                        slopeBoost = 1.2f;
                    }

                    if (slopeness < 0)
                    {
                        slopeBoost = 0.8f;
                    }
                    //Reduce by a factor of 15% to darken it
                    slopeBoost -= 0.15f;

                    color = ColorUtil.ColorMultiply3Clamped(color, slopeBoost);

                    var bufferOffset = z * this.chunkSize * 8 + offsetY * this.chunkSize * this.chunkSize * 8 + x + offsetX * this.chunkSize;
                    realData.DataContainer.PixelData[bufferOffset] = (int)(color >> 0 & 0xff | (color >> 8 & 0xff) << 8 | (color >> 16 & 0xff) << 16 | 0xff000000);
                }
            }

            //This seems weird, but there is a bug with the chunk system that will just give a depth of 0 or 1 for every block, I.E. we only get mantles.
            //If that's the case, we just want to redo this entire tile, so we return null if all height values we've checked are 1 or less.
            if (testList.All(x => x <= 1))
            {
                return false;
            }

            return true;
        }
    }
}
