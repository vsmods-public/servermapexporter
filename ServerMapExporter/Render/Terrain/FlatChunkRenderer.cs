namespace ServerMapExporter.Render.Terrain
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using System.Threading.Tasks;
    using ServerMapExporter.Models;
    using Vintagestory.API.Common;
    using Vintagestory.API.MathTools;

    public class FlatChunkRenderer : ITerrainRenderer
    {
        private readonly int chunkSize;
        private readonly IBlockAccessor blockAccessor;
        private readonly ColorManager colorManager;

        public FlatChunkRenderer(int chunkSize, IBlockAccessor blockAccessor, ColorManager colorManager)
        {
            this.chunkSize = chunkSize;
            this.blockAccessor = blockAccessor;
            this.colorManager = colorManager;
        }

        public async Task<Bitmap> RenderTerrainData(ChunkColumnExport data)
        {
            if (data.Chunks == null || data.Chunks.Length == 0)
            {
                throw new ArgumentNullException(nameof(data));
            }

            var bitmap = new Bitmap(this.chunkSize, this.chunkSize);
            var testList = new List<int>();
            for (var x = 0; x < this.chunkSize; x++)
            {
                for (var z = 0; z < this.chunkSize; z++)
                {

                    var blockPositionX = data.ChunkCoords.X * this.chunkSize + x;
                    var blockPositionZ = data.ChunkCoords.Y * this.chunkSize + z;
                    var yMax = data.ChunkColumn.RainHeightMap[MapUtil.Index2d(x, z, this.chunkSize)];
                    testList.Add(yMax);

                    if (data.Chunks[yMax / this.chunkSize].Blocks == null || data.Chunks[yMax / this.chunkSize].Blocks.Length == 0)
                    {
                        data.Chunks[yMax / this.chunkSize].Unpack();
                    }

                    var block = this.blockAccessor.GetBlock(data.Chunks[yMax / this.chunkSize].Blocks[MapUtil.Index3d(x, yMax % this.chunkSize, z, this.chunkSize, this.chunkSize)]);
                    while (block.Code.ToString().Contains("game:air") || block.Code.ToString().Contains("snowlayer") || block.Code.ToString().Contains("snowblock"))
                    {
                        if (data.Chunks[--yMax / this.chunkSize].Blocks == null || data.Chunks[yMax / this.chunkSize].Blocks.Length == 0)
                        {
                            data.Chunks[yMax / this.chunkSize].Unpack();
                        }

                        block = this.blockAccessor.GetBlock(data.Chunks[yMax / this.chunkSize].Blocks[MapUtil.Index3d(x, yMax % this.chunkSize, z, this.chunkSize, this.chunkSize)]);
                    }
                    var color = this.colorManager.GetColorFromBlockCode(block.Code.ToString());

                    bitmap.SetPixel(x, z, Color.FromArgb(color >> 16 & 0xff, color >> 8 & 0xff, color & 0xff));
                }
            }

            //This seems weird, but there is a bug with the chunk system that will just give a depth of 0 or 1 for every block, I.E. we only get mantles.
            //If that's the case, we just want to redo this entire tile.
            if (testList.All(x => x <= 1))
            {
                return null;
            }

            return bitmap;
        }

        public async Task<bool> RenderTerrainDataPart(ChunkColumnExport data, Bitmap bitmap, int offsetX, int offsetY)
        {
            var chunkRender = await this.RenderTerrainData(data);
            if (chunkRender == null)
            {
                return false;
            }

            for (var x = 0; x < chunkRender.Width; x++)
            {
                for (var y = 0; y < chunkRender.Height; y++)
                {
                    bitmap.SetPixel(offsetX + x, offsetY + y, chunkRender.GetPixel(x, y));
                }
            }

            return true;
        }
    }
}
